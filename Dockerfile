#with build dependencies
FROM node:16-alpine AS temp

#todo fixme
ENV VITE_API_URL=http://localhost:8000
ENV VITE_WS_API_URL=ws://localhost:8000/websocket

RUN npm install --location=global pnpm

WORKDIR /app

#COPY package*.json .
COPY package.json .
COPY pnpm-lock.yaml .

#RUN npm ci
RUN pnpm install --frozen-lockfile


COPY . .

#RUN npm run build
RUN pnpm run build

#RUN npm prune --production
RUN pnpm prune --production

#FROM node:12 AS stage1
#ADD . /app
#WORKDIR /app
#RUN npm install

#leave only needed to run
#FROM node:16-alpine
#FROM mhart/alpine-node:slim-16
#FROM gcr.io/distroless/nodejs:16
#FROM gcr.io/distroless/nodejs:debug
FROM nginx:1.23-alpine

#WORKDIR /app

#COPY --from=temp /app/dist build/
#COPY --from=temp /app/dist .
COPY --from=temp /app/dist /usr/share/nginx/html
#COPY --from=temp /app/dist dist/
#COPY --from=temp /app/node_modules node_modules/

#COPY package.json .

#EXPOSE 3000

#ENV NODE_ENV=production

#CMD [ "node", "build" ]
#CMD [ "node", "index.js" ]
#CMD [ "node", "dist" ]
#debug only
#CMD [ "node" ]

#FROM gcr.io/distroless/nodejs
#COPY --from=stage1 /app /app
#WORKDIR /app
#EXPOSE 8080
#CMD ["server.js"]