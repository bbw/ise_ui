import { defineConfig } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte';
import { config } from 'dotenv';


// console.log("base path: " + JSON.stringify(import.meta.env.BASE_URL));
console.log("vite env: " + JSON.stringify(import.meta.env));
console.log("config: " + JSON.stringify(config()));
console.log("env: " + JSON.stringify(process.env));

// https://vitejs.dev/config/
export default defineConfig({
// const config = {
  server: {
    cors: false,
    // cors: {
    //   origin: 'http://localhost:8080',
    //   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
    // },
    port: 5173,
    strictPort: true,
    proxy: {
      '/api': {
        target: process.env.API_URL || 'http://localhost:8000', //todo from env
        changeOrigin: true,
        // hostRewrite: process.env.API_URL || 'http://localhost:8080',
        secure: false,
        ws: false,
        // onProxyReq: function(request) {
        //   request.setHeader("origin", process.env.API_URL || "http://localhost:8000");
        // },
        // headers: {
        //   'Origin': process.env.API_URL || "http://localhost:8000"
        // },
      },
      '/websocket': {
        target: process.env.WS_API_URL || 'ws://localhost:8000', //todo from env
        // changeOrigin: true,
        // secure: false,
        ws: true,
        // configure: (proxy, options) => {
        //   // proxy will be an instance of 'http-proxy'
        // }
      },
    },
  },
  plugins: [svelte()],

});
// };

// export default config;
