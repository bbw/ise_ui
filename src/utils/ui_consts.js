import {
    CVE_ID,
    FILE_NAME, FILE_SIZE,
    HOST_NAME,
    IP_ADDRESS, OBJ_TYPE, OBJ_TYPE_CVE, OBJ_TYPE_FILE,
    OBJ_TYPE_HOST,
    OBJ_TYPE_IP_ADDRESS, OBJ_TYPE_IP_LIST, OBJ_TYPE_IP_SUBNET,
    OBJ_TYPE_PORT,
    OBJ_TYPE_SERVICE, OBJ_VALUE,
    PORT_NUMBER,
    PORT_PROTOCOL,
    PROFILE_SHORT, SERVICE_CPE,
    SERVICE_NAME,
    SERVICE_TYPE, STATUS_COMPLETED, STATUS_DELETED, STATUS_NEW, STATUS_OPEN, TEXT_CONTENT
} from "./consts.js";
import {faCheck, faEdit, faHourglass, faLeaf, faPlay, faTimes, faDownload} from "@fortawesome/free-solid-svg-icons";

export const UI_PARAM_SEARCH = 'search';

export const UI_FILTER_TYPE = "filter.type";
export const UI_FILTER_STATE = "filter.state";
export const UI_FILTER_VALUE = "filter.value";
export const UI_FILTER_SELECTED = "filter.selected";
export const UI_FILTER_UNSELECTED = "filter.unselected";
export const UI_FILTER_INCLUDE = "filter.include";
export const UI_FILTER_EXCLUDE = "filter.exclude";

export const UI_EVENT_APPSTATECHANGE = 'appstatechange';
export const UI_EVENT_FILTERSCHANGE = 'filterschange';
export const UI_EVENT_DATACHANGE = 'datachange';
export const UI_EVENT_ACTION = 'action';
export const UI_EVENT_DOWNLOAD = 'download';

export const UI_EVENT_CREATE = 'ui.event.create';
export const UI_EVENT_UPDATE = 'ui.event.update';
export const UI_EVENT_DELETE = 'ui.event.delete';

export const UI_ACTION_COMPLETE = 'ui.action.complete';
export const UI_ACTION_EDIT = 'ui.action.edit';
export const UI_ACTION_DELETE = 'ui.action.delete';
export const UI_ACTION_RUN = 'ui.action.run';
export const UI_ACTION_DOWNLOAD = 'ui.action.download';

export const UI_ICON = "ui.icon";
export const UI_COLOR = "ui.color";

export const UI_CARDS_CONFIG = {
    [OBJ_TYPE_FILE]: {
        [UI_ICON]: "images/file-with-magnifying.png",
        [PROFILE_SHORT]: [FILE_NAME],
        [OBJ_VALUE]: (obj) => `${obj[OBJ_TYPE]} : ${obj[FILE_NAME]}(${obj[FILE_SIZE]})`,
    },
    [OBJ_TYPE_IP_ADDRESS]: {
        [UI_ICON]: "images/global-location.png",
        [PROFILE_SHORT]: [IP_ADDRESS],
        [OBJ_VALUE]: (obj) => `${obj[OBJ_TYPE]} : ${obj[IP_ADDRESS] || ""}`,
    },
    [OBJ_TYPE_IP_LIST]: {
        [UI_ICON]: "images/global-location.png",
        [PROFILE_SHORT]: [TEXT_CONTENT],
        [OBJ_VALUE]: (obj) => `${obj[OBJ_TYPE]} : ${obj[TEXT_CONTENT] || ""}`,
    },
    [OBJ_TYPE_IP_SUBNET]: {
        [UI_ICON]: "images/global-location.png",
        [PROFILE_SHORT]: [TEXT_CONTENT],
        [OBJ_VALUE]: (obj) => `${obj[OBJ_TYPE]} : ${obj[TEXT_CONTENT] || ""}`,
    },
    [OBJ_TYPE_SERVICE]: {
        [UI_ICON]: "images/data-optimization.png",
        [PROFILE_SHORT]: [SERVICE_NAME, SERVICE_TYPE, IP_ADDRESS],
        [OBJ_VALUE]: (obj) => `${obj[OBJ_TYPE]} : ${obj[SERVICE_NAME] || "-"}/${obj[SERVICE_TYPE] || "-"} (${obj[IP_ADDRESS] || ""}:${obj[PORT_NUMBER] || "-"})`,
    },
    [OBJ_TYPE_PORT]: {
        [UI_ICON]: "images/cable-connector.png",
        [PROFILE_SHORT]: [PORT_NUMBER, PORT_PROTOCOL, IP_ADDRESS],
        [OBJ_VALUE]: (obj) => `${obj[OBJ_TYPE]} : ${obj[PORT_PROTOCOL] || "-"}/${obj[PORT_NUMBER] || ""} (${obj[IP_ADDRESS] || ""})`,
    },
    [OBJ_TYPE_HOST]: {
        [UI_ICON]: "images/computer.png",
        [PROFILE_SHORT]: [HOST_NAME, IP_ADDRESS],
        [OBJ_VALUE]: (obj) => `${obj[OBJ_TYPE]} : ${obj[HOST_NAME] || "-"} (${obj[IP_ADDRESS] || ""})`,
    },
    [OBJ_TYPE_CVE]: {
        [UI_ICON]: "images/bug.png",
        [PROFILE_SHORT]: [CVE_ID],
        [OBJ_VALUE]: (obj) => `${obj[OBJ_TYPE]} : ${obj[CVE_ID] || "-"}/${obj[SERVICE_CPE] || ""})`,
    },
}

export const STATUS_ICONS = {
    [STATUS_NEW]: {
        [UI_ICON]: faLeaf,
        [UI_COLOR]: '#06c109',
    },
    [STATUS_OPEN]: {
        [UI_ICON]: faHourglass,
        [UI_COLOR]: '#f1c413',
    },
    [STATUS_COMPLETED]: {
        [UI_ICON]: faCheck,
        [UI_COLOR]: '#06238c',
    },
    [STATUS_DELETED]: {
        [UI_ICON]: faTimes,
        [UI_COLOR]: '#db0e07',
    },
}

export const ACTIONS_ICONS = {
    [UI_ACTION_EDIT]: {
        [UI_ICON]: faEdit,
        [UI_COLOR]: '#0c1abb',
    },
    [UI_ACTION_COMPLETE]: {
        [UI_ICON]: faCheck,
        [UI_COLOR]: '#288c06',
    },
    [UI_ACTION_DELETE]: {
        [UI_ICON]: faTimes,
        [UI_COLOR]: '#db0e07',
    },
    [UI_ACTION_RUN]: {
        [UI_ICON]: faPlay,
        [UI_COLOR]: '#007e02',
    },
    [UI_ACTION_DOWNLOAD]: {
        [UI_ICON]: faDownload,
        [UI_COLOR]: '#428af0',
    },
}

