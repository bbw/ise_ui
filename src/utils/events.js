import {
    EVENT_DATA,
    EVENT_TYPE,
    EVENT_TYPE_OBJ_CREATE,
    EVENT_TYPE_SCRIPT_RUN,
    EVENT_TYPE_SCRIPTS_GET,
    SCRIPT_CONFIG,
    SCRIPT_NAME
} from "./consts.js";

export function createEvent(eventType, data) {
    let dataCopy = {...data}; //todo ?

    let event = {};
    event[EVENT_TYPE] = eventType;
    event[EVENT_DATA] = dataCopy;
    return event;
}

export function createRunScriptEvent(scriptName, config, data) {
    return {
        [EVENT_TYPE]: EVENT_TYPE_SCRIPT_RUN,
        [SCRIPT_NAME]: scriptName,
        [SCRIPT_CONFIG]: config,
        [EVENT_DATA]: [data]
    };
}

export function createNewObjectsEvent(data) {
    return {
        [EVENT_TYPE]: EVENT_TYPE_OBJ_CREATE,
        [EVENT_DATA]: [data]
    }
}

export function createScriptsGetEvent() {
    return {
        [EVENT_TYPE]: EVENT_TYPE_SCRIPTS_GET,
        // [EVENT_DATA]: [data]
    }
}

