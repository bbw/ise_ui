import moment from 'moment'

export const DATETIME_ORDERED = 'YYYY-MM-DD HH:mm:ss';
export const DATE_ORDERED = 'YYYY-MM-DD';

export function formatDate(dateString) {
    const date = moment(dateString, DATETIME_ORDERED);

    if (moment().isSame(date, 'day')) {
        return moment.duration(date.diff(moment(), 'milliseconds')).humanize(true);
    } else {
        return date.format(DATETIME_ORDERED);
    }

}

export function formatCurrentDate() {
    return moment().format(DATETIME_ORDERED);
}

export function getStateFromLocalStorage(storeKey, defaultValue='{}') {
    let jsonState = localStorage.getItem(storeKey) || defaultValue;

    let state;

    try {
        state = JSON.parse(jsonState);
    } catch(err) {
        console.error(err);
        state = JSON.parse(defaultValue);
    }
    return state;
}

export function saveStateToLocalStorage(storeKey, value) {
    // for reactivity purposes
    let jsonStore = JSON.stringify(value);
    // console.log('before save: ' + jsonStore);
    localStorage.setItem(storeKey, jsonStore);
}

export function isEmptyObject(obj) {
    return Object.keys(obj).length === 0;
}