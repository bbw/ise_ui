export const DEFAULT_PROJECT_ID = '-1';

export const EVENT_DATA = 'event.data';

export const DATA_OBJECTS = 'data.objects';
export const DATA_SCRIPTS = 'data.scripts';
export const DATA_ACTIONS = 'data.actions';

export const EVENT_TYPE = 'event.type';
export const EVENT_TYPE_CREATE = 'event.type.create';
export const EVENT_TYPE_OBJ_CREATE = 'event.type.obj.create';
export const EVENT_TYPE_SCRIPT_RUN = 'event.type.script.run';
export const EVENT_TYPE_SCRIPTS_GET = 'event.type.scripts.get';

export const SCRIPT_NAME = 'script.name';
export const SCRIPT_CONFIG = 'script.config';
export const SCRIPT_DATA = 'script.data';
export const SCRIPT_TYPE = 'script.type';
export const SCRIPT_GROUP = 'script.group';
export const SCRIPT_DESC = 'script.desc';
export const SCRIPT_OPTIONS = 'script.options';

export const OPTION_NAME = 'option.name';
export const OPTION_TYPE = 'option.type';
export const OPTION_VALUE = 'option.value';
export const OPTION_DESC = 'option.desc';
export const OBJ_TYPE = 'obj.type';

export const OBJ_ID = 'obj.id';
export const OBJ_NAME = 'obj.name';
export const OBJ_VALUE = 'obj.value';
export const OBJ_CREATED = 'obj.created';
export const OBJ_STATUS = 'obj.status';
export const OBJ_TAGS = 'obj.tags';

export const SCRIPT_TYPE_SEARCH = 'action.type.search';





export const PARAM_NAME = 'param.name';
export const PARAM_VALUE = 'param.value';


export const TEXT_SUBTYPE = 'text.type';
export const TEXT_SUBTYPE_TODO = 'text.todo';

export const TEXT_TITLE = 'text.title';
export const TEXT_CONTENT = 'text.content';

export const STATUS_NEW = 'status.new';
export const STATUS_OPEN = 'status.open';
export const STATUS_COMPLETED = 'status.completed';
export const STATUS_DELETED = 'status.deleted';

export const PROJECT_TAG = 'project.tag';
export const PROJECT_NAME = 'project.name';

export const OBJ_TYPE_FILE = 'os.file';
export const OBJ_TYPE_TEXT = 'media.text';
export const OBJ_TYPE_HOST = "network.host";
export const OBJ_TYPE_PORT = "network.port";
export const OBJ_TYPE_SERVICE = "network.service";
export const OBJ_TYPE_IP_ADDRESS = "network.ip.address";
export const OBJ_TYPE_IP_SUBNET = "network.ip.subnet";
export const OBJ_TYPE_IP_LIST = "network.ip.list";
export const OBJ_TYPE_CVE = "security.cve";

export const FILE_NAME = "file.name";
export const FILE_CONTENT_TYPE = "file.content.type";
export const FILE_SIZE = "file.size";

export const CVE_ID = "security.cve.id";

export const HOST_NAME = "host.name";

export const IP_ADDRESS = "ip.address";
export const IP_ADDRESS_TYPE = "ip.address.type";

export const PORT_NUMBER = "port.number";
export const PORT_PROTOCOL = "port.protocol";

export const SERVICE_NAME = "service.name";
export const SERVICE_TYPE = "service.type";
export const SERVICE_CPE = "service.cpe";

export const RESPONSE_DATA = "resp.data";
export const RESPONSE_ERR = "resp.err";

export const PROFILE_SHORT = "profile.short";