import {writable} from "svelte/store";

import {getStateFromLocalStorage, saveStateToLocalStorage} from "../utils/utils.js";

const SCRIPTS_STORE_KEY = 'scripts_store';

function createScriptsStore() {
    const _scriptsStore = writable([]);
    _scriptsStore.set(getStateFromLocalStorage(SCRIPTS_STORE_KEY, '[]'));
    _scriptsStore.subscribe((newValue) => saveStateToLocalStorage(SCRIPTS_STORE_KEY, newValue));


    return {
        subscribe: _scriptsStore.subscribe,
        set: _scriptsStore.set,
    }
}

export const scriptsStore = createScriptsStore();

// export const scriptsByObjectTypesStore = derived(
//     scriptsStore,
//     ($scripts) => {
//         const objectTypes = {};
//
//         const allScripts = $scripts;
//
//         allScripts.map((script) => {
//             let scriptObjectTypes = script[OBJ_TYPE];
//             let scriptsForSingleType = objectTypes[scriptObjectTypes];
//             if (!scriptsForSingleType) {
//                 scriptsForSingleType = [];
//                 objectTypes[]
//             }
//             FILTERED_PARAMS.map( (paramName) => {
//                 // console.log('paramName=' + JSON.stringify(paramName));
//                 const anyTypeParamValue = obj[paramName];
//                 // console.log('anyTypeParamsValue=' + JSON.stringify(anyTypeParamValue));
//                 if (anyTypeParamValue) {
//                     const allParamsValues = paramsMap[paramName] || [];
//                     // console.log('allParamsValue=' + JSON.stringify(allParamsValues));
//                     const paramValuesList = OBJ_TAGS === paramName ? anyTypeParamValue.split(',') : [anyTypeParamValue];
//                     const newParamsValues = paramValuesList.filter(paramValue => !allParamsValues.includes(paramValue));
//                     paramsMap[paramName] = [...allParamsValues, ...newParamsValues];
//                 }
//             })
//         })
//
//         return Object.keys(paramsMap).map((key) => {
//             const value = paramsMap[key];
//
//             return {
//                 [PARAM_NAME]: key,
//                 [PARAM_VALUE]: value,
//             };
//         });
//     }
// );