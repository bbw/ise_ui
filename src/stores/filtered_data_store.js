import {derived} from "svelte/store"

import {
    UI_FILTER_EXCLUDE,
    UI_FILTER_INCLUDE,
    UI_PARAM_SEARCH
} from "../utils/ui_consts.js";

import {appState} from "./app_state_store.js";

import {dataStore} from "./data_store.js";

function filterByFilter(filterInclude, filterExclude) {
    return (obj) => {
        const excludedFilterValue = filterExclude || [];
        const includedFilterValue = filterInclude || [];
        const excludeFilterOn = excludedFilterValue.length > 0;
        const includeFilterOn = includedFilterValue.length > 0;

        const filteredOutByExcludeFilter = excludeFilterOn
            ? Object.keys(obj).filter(paramName => {
                const paramValue = obj[paramName];
                const filtersExclude = paramValue
                    && excludedFilterValue.find(entry => entry[paramName] === paramValue)
                    || false;
                // console.log(`filterExclude ${filtersExclude} for ${paramName}:${paramValue} exc:${JSON.stringify(excludedFilterValue)} `);
                return filtersExclude;
            }).length > 0
        : false;
        // console.log(`filteredOutByExcludeFilter = ${filteredOutByExcludeFilter}`);

        const filteredInByIncludeFilter = !filteredOutByExcludeFilter && includeFilterOn
            ? Object.keys(obj).filter(paramName => {

                const paramValue = obj[paramName];
                const filtersInclude = paramValue
                    && includedFilterValue.length>0
                    && includedFilterValue.find(entry => entry[paramName] === paramValue)
                    || false;
                // console.log(`filterInclude ${filtersInclude} for ${paramName}:${paramValue} inc:${JSON.stringify(includedFilterValue)} `);
                return filtersInclude;
            }).length > 0
            : true;
        // console.log(`filteredInByIncludeFilter = ${filteredInByIncludeFilter}`);

        return !filteredOutByExcludeFilter && filteredInByIncludeFilter;
    };
}

function filterBySearch(searchString) {
    return (obj) => {
        return Object.keys(obj).find(paramName => obj[paramName].includes(searchString));
    };
}

export const filteredObjects = derived([appState, dataStore], ([$appState, $objectsStore]) => {
    // console.log("filter objects...")
    const allObjects = $objectsStore;
    const searchString = $appState[UI_PARAM_SEARCH]?.trim()?.toLocaleLowerCase() || '';
    const searchEmpty = searchString.length === 0;
    const filterInclude = $appState[UI_FILTER_INCLUDE] || [];
    const filterExclude = $appState[UI_FILTER_EXCLUDE] || [];

    const emptyFilters = filterInclude.length===0 && filterExclude.length===0;

    if (searchEmpty && emptyFilters) {
        return allObjects;
    } else {
        // console.log('filtered before filter=' + JSON.stringify(allObjects));
        const filteredByFilter = emptyFilters
            ? allObjects
            : allObjects.filter(filterByFilter(filterInclude, filterExclude));
        // console.log('filtered after filter=' + JSON.stringify(filteredByFilter));
        // const filteredBySearch = allTodos

        const filteredBySearch = searchEmpty? filteredByFilter : filteredByFilter.filter(filterBySearch(searchString));
        // console.log('filtered after search=' + JSON.stringify(filteredBySearch));

        return filteredBySearch;
    }
});
