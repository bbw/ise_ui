import { createBasicStore } from './basic_store';
import { createObject as apiCreate, updateObject as apiUpdate, readObjects as apiRead  } from '../api/api'

function createObjectsStore() {
    // const { subscribe, set, update} = writable([]);
    const _intStore = createBasicStore();

    async function add(params) {
        const response = await apiCreate(params);
        const list = response.get('data')?.get('new') || []; //todo check on error
        if (list.length > 0) {
            const saved = list[0];
            _intStore.add(saved);
        }
    }

    async function remove(removeId) {
        // _intStore.update({'id':removeId, 'status' : 'deleted'});
    }

    async function update(params) {
        const response = await apiUpdate(params);
        const list = response.get('data')?.get('current') || []; //todo check on error
        //todo update from saved
        _intStore.update(params);
    }

    async function reset() {
        const response = await apiRead();
        const list = response.get('data')?.get('current') || []; //todo check on error
        _intStore.set(list);
    }

    return {
        subscribe: _intStore.subscribe,
        init: reset,
        reset: reset,
        add: add,
        remove: remove,
        update: update
    }
}

export const objectsStore = createObjectsStore();