import {createBasicStore} from './basic_store';
import {
    createProject as apiCreate,
    parseObjects,
    parseSingleObject,
    readProjects as apiRead,
    updateProject as apiUpdate
} from '../api/api';

import {OBJ_ID} from "../utils/consts";

function createProjectsStore() {
    // const { subscribe, set, update} = writable([]);
    const _intStore = createBasicStore();

    resetProjects(); //todo errors

    async function addProject(newProject) {
        const responsePromise = apiCreate(newProject);
        const savedProject = await parseSingleObject(responsePromise, 'new');
        if (savedProject[OBJ_ID]) {
            _intStore.add(savedProject);
            return savedProject;
        }
        return {'error': "not saved"} //todo rework
    }

    async function removeProject(removeId) {
        // _intStore.update({'id':removeId, 'status' : 'deleted'});
    }

    async function updateProject(params) {
        const responsePromise = apiUpdate(params);
        const updatedProject = await parseSingleObject( responsePromise, 'current'); //todo check on error
        //todo update from saved
        if (updatedProject[OBJ_ID]) {
            _intStore.update(updatedProject);
            return updatedProject;
        }
        return params; //todo get from projects?
    }

    async function readProjects() {
        const responsePromise =  apiRead();
         //todo check on error
        return await parseObjects(responsePromise, 'current');
    }

    async function resetProjects() {
        const projects = await readProjects();
        _intStore.set(projects);
    }

    return {
        subscribe: _intStore.subscribe,
        init: resetProjects,
        reset: resetProjects,
        addProject: addProject,
        removeProject: removeProject,
        updateProject: updateProject
    }
}

export const projectsStore = createProjectsStore();