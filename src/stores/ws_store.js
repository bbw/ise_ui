import { writable } from 'svelte/store';

const messageStore = writable('');

const socket = new WebSocket(import.meta.env.VITE_WS_API_URL);

// Connection opened
socket.addEventListener('open', function (event) {
    // console.log("It's open");
    console.log("Socket status: " + socket.readyState)
});

// Listen for messages
socket.addEventListener('message', function (event) {
    console.log("Recieved: " + JSON.stringify(event));
    messageStore.set(event.data);
});

function sendMessage(message) {
    console.log("Sending: " + JSON.stringify(message))
    // console.log("Socket status: " + socket.readyState)
    if (socket.readyState <= 1) {
        socket.send(message);
    }
}


export default {
    subscribe: messageStore.subscribe,
    send: sendMessage
}