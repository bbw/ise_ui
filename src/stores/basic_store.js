import {writable} from 'svelte/store';
import {OBJ_ID} from "../utils/consts";


export function createBasicStore(idField = OBJ_ID) {
    // const { subscribe, set, update} = writable([]);
    const _intStore = writable([]);

    function addObject(newObject) {
        _intStore.update(objects => [...objects, newObject]);
    }

    function removeObject(removeId) {
        console.log("removing id=" + removeId);
        _intStore.update(objects => objects.filter(object => object[idField] !== removeId));
    }

    function updateObject(newObjectParams, addNotFound = true) {
        // console.log(`new obj params=${JSON.stringify(newObjectParams)}`);
        // _intStore.update(n => n.map => n.id === props.id ? {...n, ...props} : n);

        let updated = false;
        _intStore.update(objects => objects
            .map(object => {
                    const found = object[idField] === newObjectParams[idField]
                    updated = found || updated;
                    return found ? {...object, ...newObjectParams} : object
                }
            ));
        // console.log(`addNotFound=${addNotFound} and updated=${updated}`);
        if (addNotFound && !updated) {
            addObject(newObjectParams);
        }
    }

    return {
        subscribe: _intStore.subscribe,
        set: _intStore.set,
        clear: () => _intStore.set([]),
        add: addObject,
        remove: removeObject,
        update: updateObject
    }
}

