import {derived} from "svelte/store";

import {createBasicStore} from './basic_store';

import {
    OBJ_CREATED,
    OBJ_ID,
    OBJ_TAGS, OBJ_VALUE,
    PARAM_NAME,
    PARAM_VALUE,
} from "../utils/consts.js";

// import {getStateFromLocalStorage, saveStateToLocalStorage} from "../utils/utils.js";

// const OBJECTS_STORE_KEY = 'objects_store';

const FILTERED_OUT_PARAMS = [
    OBJ_ID,
    OBJ_CREATED,
    OBJ_VALUE,
];

function createDataStore() {
    const _objectsStore = createBasicStore();

    // const { subscribe, set, update} = writable([]);
    _objectsStore.set([]);
    // _objectsStore.set(getStateFromLocalStorage(OBJECTS_STORE_KEY, '[]'));
    // _objectsStore.subscribe((newValue) => saveStateToLocalStorage(OBJECTS_STORE_KEY, newValue));

    async function addObject(newObject) {
        console.log("add object")
        _objectsStore.add(newObject);
        return newObject;
    }

    async function removeObject(removeId) {
        console.log("remove object " + removeId)
        // _intStore.update({[OBJ_ID]: removeId, [OBJ_STATUS]: STATUS_DELETED});
        _objectsStore.remove(removeId);
    }

    async function updateObject(params) {
        console.log("update object")
        _objectsStore.update(params);
        return params;
    }


    // function initStore(state = []) {
    //     console.log(`init store by=${JSON.stringify(state)}`);
    //     _objectsStore.set(state);
    //     return state;
    // }

    return {
        subscribe: _objectsStore.subscribe,
        // init: initStore,
        // reset: initStore,
        add: addObject,
        update: updateObject,
        remove: removeObject,
    }
}

export const dataStore = createDataStore();

export const paramsValuesStore = derived(
    dataStore,
    ($objectsStore) => {
        console.log("recalculating filters...");
        const paramsMap = {};

        // const allObjects = get(todosStore);
        // console.log('todosStore=' + JSON.stringify($objectsStore));
        const allObjects = $objectsStore;

        const allObjectsKeys = new Set();
        allObjects.forEach(obj => Object.keys(obj)
            .forEach(field => {
                if (!FILTERED_OUT_PARAMS.includes(field)) allObjectsKeys.add(field)
            }));

        allObjects.forEach((obj) => {
            allObjectsKeys.forEach((paramName) => {
                // console.log('paramName=' + JSON.stringify(paramName));
                const anyTypeParamValue = obj[paramName];
                // console.log('anyTypeParamsValue=' + JSON.stringify(anyTypeParamValue));
                if (anyTypeParamValue) {
                    const allParamsValues = paramsMap[paramName] || [];
                    // console.log('allParamsValue=' + JSON.stringify(allParamsValues));
                    const paramValuesList = OBJ_TAGS === paramName ? anyTypeParamValue.split(',') : [anyTypeParamValue];
                    const newParamsValues = paramValuesList.filter(paramValue => !allParamsValues.includes(paramValue));
                    paramsMap[paramName] = [...allParamsValues, ...newParamsValues];
                }
            })
        })

        return Object.keys(paramsMap).map((key) => {
            const value = paramsMap[key];

            return {
                [PARAM_NAME]: key,
                [PARAM_VALUE]: value,
            };
        });
    }
);