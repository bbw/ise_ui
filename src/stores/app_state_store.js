import { get, writable } from 'svelte/store';
// import {getStateFromLocalStorage, saveStateToLocalStorage} from "../utils/utils.js";

// const STORE_KEY = 'app_state';


function createAppLocalState() {

    // const { subscribe, set, update} = writable([]);
    const _intStore = writable([]);
    // const _intStore = writable(getStateFromLocalStorage(STORE_KEY));
    // _intStore.subscribe((newValue) => saveStateToLocalStorage(STORE_KEY, newValue));


    console.log('int store: ' + JSON.stringify(get(_intStore)));

    async function setParams(newParams) {
        console.log('old state:' + JSON.stringify(get(_intStore)));
        console.log('new params:' + JSON.stringify(newParams));
        const newState = {...get(_intStore), ...newParams};
        console.log('new state:' + JSON.stringify(newState));
        _intStore.set(newState);
        return newState;
    }

    async function getParam(key, defaultValue) {
        const currentValue = get(_intStore)[key] || defaultValue;
        console.log(`current param: ${JSON.stringify(currentValue)}`);

        return currentValue;
    }

    async function setParam(key, value) {
        const newState = get(_intStore);
        newState[key] = value;
        console.log(`set param new state: ${JSON.stringify(newState)}`);
        _intStore.set(newState);
        return newState;
    }


    async function addParam(paramName, newParamValue) {
        console.log(`add param: ${paramName}:${JSON.stringify(newParamValue)}`);
        const oldParam = await getParam(paramName, []);
        const newParam = [...oldParam, newParamValue];
        return setParam(paramName, newParam);
    }

    async function removeParam(paramName, removeParamValue) {
        console.log('remove param:' + JSON.stringify(removeParamValue));
        const keys = Object.keys(removeParamValue);
        const key= keys[0];
        const value = removeParamValue[keys[0]];
        const oldParam = await getParam(paramName, []);
        // console.log(`removing '${key}':'${value}' from ${JSON.stringify(oldParam)}`);
        const newParamsValue = oldParam.filter(entry => !(entry[key] === value));
        return setParam(paramName, newParamsValue);
    }

    async function clearState() {
        _intStore.set({});
    }


    return {
        subscribe: _intStore.subscribe,
        reset: clearState,
        // setParam: setParam,
        // getParam: getParam,
        setParams: setParams,
        addParam: addParam,
        removeParam: removeParam,
        // handleAppStateChangeEvent: handleAppStateChangeEvent,
    }
}

export const appState = createAppLocalState();