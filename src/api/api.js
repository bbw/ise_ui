import {FILE_CONTENT_TYPE, FILE_NAME} from "../utils/consts.js";
import download from "downloadjs"

const apiHost = import.meta.env.VITE_API_URL || 'http://localhost:8080'; //todo from env

console.log(apiHost)

function callApi(method, resource, body) {
    return fetch(`${apiHost}${resource}`, {
        method,
        headers: {
            // 'Authorization': 'Bearer testtesttest', //todo replace later
            'content-type': 'application/json',
        },
        body: body && JSON.stringify(body)
    });
}

async function downloadFile(fileInfo) {
    console.log("api downloading: " + JSON.stringify(fileInfo));
    callApi('POST','/api/file/download', fileInfo)
        .then(response => response.blob())
        .then(data => download(data, fileInfo[FILE_NAME], fileInfo[FILE_CONTENT_TYPE]))
        .catch( err => console.log("error during fetch:" + err));
}

async function parseObjects(responsePromise) {
    const response = await responsePromise;
    return response['data'] ? response['data'] : []; //todo process errors
}

async function parseSingleObject(responsePromise) {
    const objects = await parseObjects(responsePromise);
    return objects.length>0 ? objects[0] : {}; //todo check on error
}

async function readObjects() {
    let response = await  callApi('GET','/api/v1/objects/list', null);
    return await response.json();
}

async function createObject(params) {
    let response = await callApi('POST','/api/v1/objects/create', params);
    return await response.json();
}

async function updateObject(params) {
    let response = await callApi('GET','/api/v1/objects/actions', params);
    return await response.json();
}

export const api = {
// export default {
    downloadFile: downloadFile,
}
