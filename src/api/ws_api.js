const WS_URL = import.meta.env.VITE_WS_API_URL;
const RECONNECT_TIMEOUT = 1000;
const RETRY_TIMEOUT = 2000;

let webSocket = connect(WS_URL);

let subscriptions = [];

function onConnect(event) {
    console.log("websocket open");

}
function onMessage(event) {
    console.log("Recieved: " + JSON.stringify(event));
    const message = JSON.parse(event.data);
    subscriptions.forEach(subscription => subscription(message));

}

function reconnect(url) {
    console.log("reconnecting...");
    webSocket = undefined;
    setTimeout(() => {
        webSocket = connect(url)
    }, RECONNECT_TIMEOUT);
}

function onClose(event) {
    console.log("websocket closed");
    // reconnect();
}

function close() {
    console.log("closing");
    if (webSocket) {
        webSocket.close();
    }
    webSocket = undefined;
}

function connect(url) {
    const newWebSocket = new WebSocket(url);

    newWebSocket.addEventListener('open', onConnect);
    newWebSocket.addEventListener('message', onMessage);
    newWebSocket.addEventListener('close', onClose);

    return newWebSocket;
}

function sendMessage(message) {
    const jsonMessage = JSON.stringify(message);
    console.log("Sending: " + jsonMessage);
    console.log("Socket status: " + webSocket.readyState);

    if (webSocket.readyState === WebSocket.OPEN) { //else sleep?
        webSocket.send(jsonMessage);
        console.log("Sent");
    } else {
        console.log("cant send, ws is down...")
        reconnect(WS_URL);
        setTimeout(() => {
            console.log("retry send...")
            sendMessage(message);
        }, RETRY_TIMEOUT);
    }
}

function subscribe(subscription) {
    subscriptions.push(subscription);
    return () => {
        subscriptions.delete(subscription);
    }
}

export const wsApi = {
// export default {
    subscribe: subscribe,
    send: sendMessage
}