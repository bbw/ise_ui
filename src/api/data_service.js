import {
    DATA_OBJECTS,
    DATA_SCRIPTS,
    OBJ_TYPE,
    RESPONSE_DATA
} from "../utils/consts";

let _api;
let _wsApi;
let _dataStore;
let _scriptsStore;

async function sendEvent(message) {
    _wsApi.send(message); //todo process error
}


async function parseMessage(messageData) {
    console.log("Parsing data: " + JSON.stringify(messageData));
    const event = messageData[RESPONSE_DATA] || {};
    if (event[DATA_OBJECTS]) {
        const objects = event[DATA_OBJECTS];
        objects.forEach( obj => { if ( obj[OBJ_TYPE] ) _dataStore.update(obj) });
    }
    if (event[DATA_SCRIPTS]) {
        const scripts = event[DATA_SCRIPTS];
        _scriptsStore.set(scripts);
    }
}

async function downloadFile(fileInfo) {
    console.log("dataservice downloadFile");
    _api.downloadFile(fileInfo);
}

function init(api, wsApi, dataStore, scriptsStore) {
    _api = api;
    _wsApi = wsApi;
    _dataStore = dataStore;
    _scriptsStore = scriptsStore;
}

export const dataService = {
// export default {
    init: init,
    parse: parseMessage,
    sendEvent: sendEvent,
    downloadFile: downloadFile,
}